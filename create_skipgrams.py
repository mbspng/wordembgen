"""
Removes stop  words and lemmatizes. Generates  all possible ngrams from  adjacent filtered
words.  Creates 4  HDFStores with  one  dataframe of  `n`  columns each, where  the ith  column
contains all tokens that appeard in any ngram at position i.
"""

import spacy
import pandas as pd
from pandas.api.types import union_categoricals
from itertools import islice
import dask.dataframe as ddf
from eta import ETADisplay
import signal
import time
import numpy as np

MAX_STRING_LENGTH = 70 # max length of strings for HDF stores
EMPTY_LINE_COUNT = 0 # number of empty lines between two lines of data
NUM_LINES = None # total number of lines (inclduing empty) in corpus file
FREQ_THRESHOLD = 0
MIN_LINE_LENGTH = 0
EXCLUDED_CHARS = []

def time_to_display():
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))

def ngram_iter(sequences, window_size=3):
    with open('log/sequences.log', 'a') as log:
        for j, sequence in enumerate(sequences):
            # if (len(sequence) - window_size +1) < 0:
            #     print(f'skipping sequence {sequence}: insufficient length')
            log.write(f'{j}\t{sequence}\n\n')
            for i in range(len(sequence) - window_size + 1):
                ngram = sequence[i:i + window_size]
                yield ngram


def save_batchwise(store, items, dataframe_name, batch_size=1000):

    batch = None  # stores $batch_size items

    def save_batch():
        data = pd.DataFrame(
            batch, columns=[f'pos_{i}' for i in range(len(batch[0]))])
        try:
            store.append(dataframe_name, data, min_itemsize=MAX_STRING_LENGTH)
        except ValueError as e:
            print(f"Skipping batch... Error: {e}")

    for i, _ in enumerate(iter(int, 1)):
        batch = list(islice(items, batch_size))
        if batch == []:
            break
        save_batch()

    df = store[dataframe_name]
    # drop NaN rows. These can stem from corrupted source data.
    # for instance:
    # $ grep -rnw "Leabhraichean" data/tatoeba_en.txt
    # 664033:You will get an interesting book if you go to Comhairle nan Leabhraichean.
    df = df.dropna()
    # fix up indices. currently only ranging from 0 to $batch_size
    # also messed up from dropping NaNs
    df = df.reset_index(drop=True)
    store.put(dataframe_name, df, complevel=9,
              complib='blosc:lz4',
              format='table'
              )

def eta_decorator(func):

    eta_display = ETADisplay(NUM_LINES)
    interval = 1000
    current = 0

    def inner(*args, **kwargs):

        nonlocal current
        current += EMPTY_LINE_COUNT+1

        if not current % interval:

            eta_display.update(interval)

            print(f"{current} lines processed | {eta_display.get_joined_display()}", end='\n')

        return func(*args, **kwargs)

    return inner


def make_process_function():

    # nlp = spacy.load('de_core_news_sm')
    nlp = spacy.load('en')
    nlp.remove_pipe('parser')
    nlp.remove_pipe('ner')

    def handler(signum, frame):
        raise Exception("nlp timed out")

    signal.signal(signal.SIGALRM, handler)

    @eta_decorator
    def process(line):
        signal.alarm(3)
        try:
            ret = nlp(line)
            signal.alarm(0)
            return ret
        except Exception as exc:
            with open('log/nlp_exceptions.log', 'w') as log:
                log.write(f'{time_to_display()}:\t{line}\n\n')
            print('here')
            print(exc)
            print(f'text passed to nlp: {line}')
            return []

    return process


def main(filename, hdfstore_filename, window_size, min_line_length=MIN_LINE_LENGTH, excluded_characters=EXCLUDED_CHARS):

    relevant_pos = ['ADJ', 'ADV', 'NOUN', 'PROPN', 'VERB']

    def p(token): return token.is_punct or token.is_stop or token.is_space

    def relevant(token): return token.pos_ in relevant_pos and not p(token)

    process = make_process_function()

    with open(filename) as f:
        lines = (line.strip() for line in f)

        # filter the corpus down to sequences of lemmata of meaningful words
        sequences = filter(None, map(lambda line: list(filter(None, map(
            lambda token: token.lemma_ if relevant(token) else None, process(line)))),
            filter(lambda line: len(line) >= min_line_length and line[0] not in excluded_characters, lines)))

        store = pd.HDFStore(hdfstore_filename+'.h5', complevel=9,
                            complib='blosc:lz4',
                            # format='table'
                            )
        save_batchwise(store, ngram_iter(sequences, window_size), 'ngrams')

    store.close()

    categorize(hdfstore_filename)

    # filter_low_frequency(hdfstore_filename+'_cat')

    # merge_categories(hdfstore_filename+'_cat_filtered')

    merge_categories(hdfstore_filename+'_cat')

def filter_low_frequency(hdfstore_filename, threshold=FREQ_THRESHOLD):
    # remove columns from dataframe, that contain words
    # with a frequency of less than $threshold
    print(f'{time_to_display()}: filtering data for low frequency words')

    store = pd.HDFStore(f'{hdfstore_filename}.h5')
    df = store.select(key='/ngrams')

    unique, counts = np.unique(df.values.ravel(), return_counts=True)
    d = dict(zip(unique, counts))

    to_remove = {k for k, v in d.items() if v < threshold}

    # make mask for all cells containing infrequent words
    mask = df.isin(to_remove)

    # make mask for all rows containing cells containing infrequent words
    mask = (~mask).all(axis=1)

    # apply mask
    df = df[mask]
    # reset index to compensate for dropped lines
    df = df.reset_index(drop=True)

    for col in df:
        df[col] = df[col].cat.remove_unused_categories()

    df.to_hdf(f'{hdfstore_filename}_filtered.h5', 'ngrams', complevel=9, complib='blosc:lz4',
            format='table'
            )

def categorize(hdfstore_filename):
    # categorize all columns
    print(f'{time_to_display()}: categorizing data')

    ngrams = ddf.read_hdf(hdfstore_filename+'.h5', '/ngrams')

    ngrams = ngrams.categorize()

    ddf.to_hdf(ngrams, hdfstore_filename+'_cat.h5', '/ngrams',
               min_itemsize=MAX_STRING_LENGTH, complevel=9, complib='blosc:lz4')


def merge_categories(hdfstore_filename):
    # merge categorical mappings of individual columns and assign it to each column
    print(f'{time_to_display()}: merging categories')

    store = pd.HDFStore(f'{hdfstore_filename}.h5')
    ngrams = store.ngrams

    all_cats = union_categoricals(
        [pd.Categorical(ngrams[col].cat.categories) for col in ngrams])

    ngrams = ngrams.apply(
        lambda col: col.cat.set_categories(all_cats.categories), axis=0)

    ngrams.to_hdf(f'{hdfstore_filename}_merged.h5', 'ngrams', complevel=9,
                  complib='blosc:lz4',
                  format='table'
                  )

    store.close()


if __name__ == '__main__':
    import subprocess
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('datafile')
    parser.add_argument('hdfstore_filename')
    parser.add_argument('context_size', type=int)
    args = parser.parse_args()

    output = subprocess.check_output(f'wc -l {args.datafile}', shell=True)
    NUM_LINES = int(output.split()[0])
    
    main(args.datafile, args.hdfstore_filename, args.context_size*2+1)