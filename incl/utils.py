class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__
    
    
def is_numeral(string):
    try:
        int(string)
        return True
    except:
         pass
    try:
        float(string)
        return True
    except:
        return False
    
from itertools import islice
import pandas as pd

def save_batchwise(store, items, dataframe_name, batch_size=1000, columns_to_categorize=[]):

    batch = None  # stores $batch_size items

    def save_batch():
        # https://docs.python.org/2/library/stdtypes.html#dict.items
        # the order of keys and values will match here, under the assumption
        # there have been made no modificatiosn to the dictionary
        values = list(map(lambda x : x.values(), batch))
        columns = batch[0].keys()
        data = pd.DataFrame(
            values, columns=columns)
        try:
            store.append(dataframe_name, data, min_itemsize=10)
        except ValueError as e:
            print(f"Skipping batch... Error: {e}")

    for i, _ in enumerate(iter(int, 1)):
        batch = list(islice(items, batch_size))
        if batch == []:
            break
        save_batch()

    # fix up indices. currently only ranging from 0 to $batch_size
    df = store[dataframe_name]
    df = df.reset_index(drop=True)
    
    for column in columns_to_categorize:
        df[column] = df[column].astype('category')     
    
    store.put(dataframe_name, df, complevel=9,
              complib='blosc:lz4', format='table')