import pandas as pd
import pickle
from IPython.display import display, HTML
import pickle
import os
import sqlite3
import pickle


pd.options.display.max_columns = None

def store_model_to_sql_database(sql_database_path, record):

    conn = sqlite3.connect(sql_database_path)
    c = conn.cursor()


    c.execute("""CREATE TABLE IF NOT EXISTS models (
                name TEXT,
                time_stamp INTEGER,
                model_name TEXT,
                model_path TEXT,
                checkpoint_path TEXT,
                optimizer TEXT,
                loss TEXT,
                num_epochs_max INTEGER,
                num_epochs_actual INTEGER,
                batch_size INTEGER,
                alphabet TEXT,
                text_length INTEGER,
                categories TEXT,
                acc REAL,
                val_accs TEXT,
                max_val_acc REAL,
                max_val_acc_idx INTEGER,
                history_figure_path_pdf TEXT,
                history_figure_path_png TEXT,
                history_pickle_path TEXT,
                filter_cnt INTEGER,
                dense_dims INTEGER,
                dropout_rate INTEGER,
                output_dims INTEGER,
                num_training_items INTEGER,
                comment TEXT,
                description_data TEXT,
                description_model TEXT,
                description_encoding TEXT,
                training_time INTEGER,
                encoding_dims INTEGER,
                embedding_layer_trainable BOOLEAN,
                char_embedding_path TEXT,
                DataGenerator_type TEXT,
                center BOOLEAN,
                normalized BOOLEAN,
                standardized BOOLEAN,
                raunak BOOLEAN,
                rightsingularvectors BOOLEAN,
                modeltype TEXT,
                X_encoder_pickle_path TEXT,
                Y_encoder_pickle_path TEXT,
                train_idx TEXT,
                test_idx TEXT,
                val_idx TEXT,
                brier_score REAL,
                f1_score REAL,
                precision REAL,
                recall REAL,
                report_str TEXT,
                fold INTEGER,
                data_path TEXT
             )""")

    with conn:
        c.execute(
            """
            INSERT INTO models VALUES (
            :name,
            :time_stamp,
            :model_name,
            :model_path,
            :checkpoint_path,
            :optimizer,
            :loss,
            :num_epochs_max,
            :num_epochs_actual,
            :batch_size,
            :alphabet,
            :text_length,
            :categories,
            :acc,
            :val_accs,
            :max_val_acc,
            :max_val_acc_idx,
            :history_figure_path_pdf,
            :history_figure_path_png,
            :history_pickle_path,
            :filter_cnt,
            :dense_dims,
            :dropout_rate,
            :output_dims,
            :num_training_items,
            :comment,
            :description_data,
            :description_model,
            :description_encoding,
            :training_time,
            :encoding_dims,
            :embedding_layer_trainable,
            :char_embedding_path,
            :DataGenerator_type,
            :center,
            :normalized,
            :standardized,
            :raunak,
            :rightsingularvectors,
            :modeltype,
            :X_encoder_pickle_path,
            :Y_encoder_pickle_path,
            :train_idx,
            :test_idx,
            :val_idx,
            :brier_score,
            :f1_score,
            :precision,
            :recall,
            :report_str,
            :fold,
            :data_path
            )""",
            record)

    conn.close()


def store_model_to_sql_database_wordembedding(sql_database_path, record):

    conn = sqlite3.connect(sql_database_path)
    c = conn.cursor()

    c.execute("""CREATE TABLE IF NOT EXISTS models (
                name TEXT,
                time_stamp INTEGER,
                model_name TEXT,
                model_path TEXT,
                optimizer TEXT,
                loss REAL,
                num_epochs_max INTEGER,
                num_epochs_actual INTEGER,
                batch_size INTEGER,
                acc REAL,
                val_acc REAL,
                history_image_path_pdf TEXT,
                history_image_path_png TEXT,
                history_pickle_path TEXT,
                num_training_items INTEGER,
                comment TEXT,
                description_data TEXT,
                description_model TEXT,
                description_encoding TEXT,
                training_time INTEGER
             )""")

    with conn:
        c.execute(
            """
            INSERT INTO models VALUES (
            :name,
            :time_stamp,
            :model_name,
            :model_path,
            :optimizer,
            :loss,
            :num_epochs_max,
            :num_epochs_actual,
            :batch_size,
            :acc,
            :val_acc,
            :history_image_path_pdf,
            :history_image_path_png,
            :history_pickle_path,
            :num_training_items,
            :comment,
            :description_data,
            :description_model,
            :description_encoding,
            :training_time
            )""",
            record)

    conn.close()

def store_model_to_hdf_database(hdf_database_path, record):

    store = pd.HDFStore(hdf_database_path, 'a')
    df = pd.DataFrame.from_records([record])
    display(df)
    store.append('data', df)
    store.close()
