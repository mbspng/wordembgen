from keras.utils import Sequence
import pandas as pd
import numpy as np
import spacy
from nltk.corpus import wordnet as wn
import random

# for multiprocessing this call is necessary. See:
# https://stackoverflow.com/questions/27433370/what-would-cause-wordnetcorpusreader-to-have-no-attribute-lazycorpusloader/27437149#27437149
print(wn.__class__)            # <class 'nltk.corpus.util.LazyCorpusLoader'>
wn.ensure_loaded()            # first access to wn transforms it
print(wn.__class__)            # <class 'nltk.corpus.reader.wordnet.WordNetCorpusReader'>
    
def naive_synonym_substitution(tokens):
    """
    This is a naive apporach to synonym substitution. The synonyms chosen are not guaranteed
    to be the correct synonym in the given context. morphology is neglected as well.
    
    tokens : spacy nlp sequence
    """
    text = [t.text for t in tokens]
    list_of_synsets = list(map(lambda t : wn.synsets(t.lemma_), tokens))
    non_empty_idcs = map(lambda x : (lambda i, ss : i if ss != [] else None)(*x), enumerate(list_of_synsets))
    non_empty_idcs = list(filter(None, non_empty_idcs))
    
    
    # from a geometric distribution pull the number of substitutions
    num_subs = min(np.random.geometric(p=0.5, size=1)[0], len(non_empty_idcs))
    # chose $num_subs at random from the non empty synsets
    try:
        sub_idcs = random.sample(non_empty_idcs, num_subs)
    except Exception as e:
        print(num_subs)
        print(non_empty_idcs)
        raise e
    for sub_idx in sub_idcs:
        try:
            synset =  list_of_synsets[sub_idx]
            synonyms = [lemma.name() for lemma in random.choice(synset).lemmas()]
            # multi word expressions are joined by underscores. remove these
            synonyms = [' '.join(s.split('_')) for s in synonyms]
            syn_idx = min(np.random.geometric(p=0.5, size=1)[0], len(synonyms)-1)
            text[sub_idx] = synonyms[syn_idx].lower()
        except Exception as e:
            print(synset)
            raise e
        
    return ' '.join(text)


def shuffle_in_unison(a, b):
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)
    
    
class AmazonReviewAugmentedDataGenerator(Sequence):
    def __init__(self,
                 data: pd.DataFrame,
                 text_length: int,
                 X_encoder,
                 Y_encoder,
                 random_span=False,
                 batch_size: int = 128,
                 shuffle=True,
                 augment=True):
        """
        data generator for the amazon review data. expects fields `summary`, `text`, `category`
        data : data frame containing items to feed to network
        batch_size : batch size
        text_length : length for text (amazon review text) of items
        random_span : whether to (if even possible) select the text as a random
                      span from the base text (of length $text_lenghth) or to start
                      from the beginning of the text
        """
        nlp = spacy.load('en')
        nlp.remove_pipe('tagger')
        nlp.remove_pipe('parser')
        nlp.remove_pipe('ner')
        
        self.nlp = nlp
        self.data = data
        self.X_encoder = X_encoder
        self.Y_encoder = Y_encoder
        self.batch_size = batch_size
        self.length = len(self.data)//self.batch_size
        self.shuffle = shuffle
        self.augment = augment

    def on_epoch_end(self):
        if self.shuffle:
            self.data = self.data.sample(frac=1).reset_index(drop=True)

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        batch_items = self.data.iloc[idx * self.batch_size:idx * self.batch_size+self.batch_size]
        
        X = (batch_items.summary + ' :: ' + batch_items.text).values
        
        X = list(map(self.nlp, X))
        
        if self.augment:
            X_aug = list(map(naive_synonym_substitution, X))
            
        X = list(map(lambda x : ' '.join([t.text for t in x]), X))
        
        if self.augment:
             X+=X_aug
            
        X = list(map(self.X_encoder, X))
        

        # for word embedding layer
        X = np.vstack(X)
        
        Y = batch_items.category.cat.codes.values
        
        if self.augment:
            Y = np.concatenate([Y,Y], axis=0)
            
        Y = self.Y_encoder(Y)
        Y = np.transpose(Y)
        
        if self.augment:
            shuffle_in_unison(X, Y)
            
        return X, Y