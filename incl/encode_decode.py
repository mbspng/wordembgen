import numpy as np
from typing import Container
from subprocess import check_output
import nltk
from incl.utils import is_numeral
import re


arpabet2ipa = {
'AA':'ɑ',  'AE':'æ',  'AH':'ʌ',  'AO':'ɔ', 'AW':'aʊ',
'AX':'ə',  'AXR':'ɚ', 'AY':'aɪ', 'EH':'ɛ', 'ER':'ɝ',
'EY':'eɪ', 'IH':'ɪ',  'IX':'ɨ',  'IY':'i', 'OW':'oʊ',
'OY':'ɔɪ', 'UH':'ʊ',  'UW':'u',  'UX':'ʉ', 'B':'b',
'CH':'tʃ', 'D':'d',   'DH':'ð',  'DX':'ɾ', 'EL':'l̩',
'EM':'m̩',  'EN':'n̩',  'F':'f',   'G':'ɡ',  'HH':'h',
'H':'h',   'JH':'dʒ', 'K':'k',   'L':'l',  'M':'m',
'N':'n',   'NG':'ŋ',  'NX':'ŋ',  'NX':'ɾ̃', 'P':'p',
'Q':'ʔ',   'R':'ɹ',   'S':'s',   'SH':'ʃ', 'T':'t',
'TH':'θ',  'V':'v',   'W':'w',   'WH':'ʍ', 'Y':'j',
'Z':'z',   'ZH':'ʒ' }

arpabet=nltk.corpus.cmudict.dict()


def transscribe_with_arpabet(word, fallback_to_espeak=False):
    # try to transscribe word
    try:
        # get aparbet transcription
        phones_ = arpabet[word]
        # choose one transcription of there are multiple
        phones_ = phones_[0] if type(phones_[0]) is list else phones_
        # translate aparbet encding to IPA
        phones = []
        for phone in phones_:
            ipa_phone = arpabet2ipa[re.sub("\d+", "", phone)]
            # aparbet transcrbes diphthongs as atomic symbols
            # resolve these to individual IPA symbols
            for p in ipa_phone:
                phones.append(p)
    # if fail, translate as Unkwon
    except Exception as e:
        if fallback_to_espeak:
            phones = transscribe_with_espeak(word)
        else:
#             raise Exception(f'transcription failed: {word}')
            phones = list(word)
        
    return phones


def transcribe_with_arpabet(word):
    # get aparbet transcription
    phones = arpabet[word]
    # choose one transcription of there are multiple
    phones = phones[0] if type(phones[0]) is list else phones
    # translate aparbet encding to IPA
    phones = [p for phone in phones for p in arpabet2ipa[re.sub("\d+", "", phone)]]
    return phones


def transscribe_with_espeak(word):
    
    phones = list(check_output(["espeak", "-q", "--ipa",
                       '-v', 'en-us',
                       word]).decode('utf-8'))
    # remove phonetic details
    phones = list(filter(lambda x : x not in ['ˈ', 'ː', 'ˌ'], phones))[1:-1]
    
    if len(phones) == 0 or is_numeral(word):
        phones = list(word)
        
    return phones


from incl.eta import eta_decorator
import pickle

with open('data/espeak_transcriptions.pkl', 'rb') as f:
    espeak_transcriptions = pickle.load(f)

def transcribe(word):
    """
    word : spacy token
    alphabet is assumed to contain all symbols that can be returned
    from transscribe_with_arpabet.
    """
    global espeak_transcriptions
    
    wlower = word.text.lower()
    
    try:
        return transcribe_with_arpabet(wlower)
    except:
        pass
    if (word.is_punct or word.is_digit
    or word.is_space or word.is_quote
    or word.is_bracket or word.like_num
    or word.is_currency or wlower in ['$', '+']):
        return list(wlower)
    try:
        trans = espeak_transcriptions[wlower]
        return trans
    except:
        try:
            trans = transscribe_with_espeak(wlower)
            espeak_transcriptions[wlower] = trans
            return trans
        except Exception as e:
            with open('transcription_log.txt', 'a') as f:
                f.write(wlower+'\n')
            raise e


import spacy

nlp = spacy.load('en')
nlp.remove_pipe('tagger')
nlp.remove_pipe('parser')
nlp.remove_pipe('ner')


def transcribe_(x):
    try:
        return transcribe(x)
    except Exception as e:
        return ''
    
import pickle

text2phones_call = 0
def text2phones(text: str):
    global espeak_transcriptions
    global text2phones_call
    text2phones_call+=1
    if not text2phones_call % 10000:
        with open ('data/espeak_transcriptions.pkl', 'wb') as f:
            pickle.dump(espeak_transcriptions, f)
    
    tokens = [tkn for tkn in nlp(text)]
    tokens_phonetic = [''.join(transcribe_(tkn)) for tkn in tokens]
    text_phonetic = ' '.join(tokens_phonetic)
    return text_phonetic


def text2indices(text: str, char2index: dict, text_length: int) -> np.array:
    
    # get first $text_length chars and reverse their order
    text = text.lower()[:text_length][::-1]

    def char2index_(char):
        try:
            return char2index[char]
        except:
            return -1 
    # transform chars to their ids.
    idcs = list(map(char2index_, text))
    # pad with -1s if $text was shorter than $text_length
    idcs = np.array(idcs+[-1]*(text_length-len(idcs)))
    return idcs
    

def text2onehot(text: str, char_id_lookup: dict, text_length: int) -> np.array:
    """
    Converts a given text into a matrix of one hot vectors, in correspondence
    with the mapping of characters to IDs defined in $char_id_lookup
    
    text: text to encode as a matrix of 1-hot vectors, one column per character
    
    char_id_lookup : assumed to start from 0 and be continuous in the naturals
    
    text_length : lenght to truncate $text to, if necessary. If text is shorter
                  than $text_length, fills up remaining columns in returned matrix
                  with zeros
    """
    dimensions = len(char_id_lookup)+1
    text = text.lower()[:text_length][::-1]

    def char2id(char):
        try:
            return char_id_lookup[char]
        except:
            return -1
    # transform chars to their ids. increment ids by 1 to account for
    # the following indexing code that will put ones in the 0th row
    # for unkown chars
    ids = np.array(list(map(lambda x: x+1, map(char2id, text))))
    encoded_text = np.zeros((dimensions, text_length))
    encoded_text[ids, np.arange(len(text))] = 1

    # cut off the first row as that correponds to unkown chars
    return encoded_text[1:, :]


def onehot2text(matrix: np.array, id_char_lookup: Container, unkown='?') -> str:
    """
    converts a matrix of hone hot vectors to a string in correspondence with the ID
    to character mapping $id_char_lookup
    """
    def id2char(id):
        try:
            return id_char_lookup[id]
        except:
            return unkown

    ids = np.argmax(matrix, axis=0)
    return ''.join(reversed(list(map(id2char, ids))))


def text2charvecs(text, embedding_matrix, char2index, text_length) -> np.array:
    
    text = text.lower()[:text_length][::-1]
    dimensions = embedding_matrix.shape[1]
    
    def char2vec(char):
        try:
            return embedding_matrix[char2index[char]]
        except:
            return np.zeros(dimensions)
        
    vecs = np.array(list(map(char2vec, text))).T
    encoded_text = np.concatenate([vecs, np.zeros((dimensions, text_length-vecs.shape[1]))], axis=1)
    
    return encoded_text


def cat2onehot(categories: np.array, num_categories: int) -> np.array:
    """
    convert array of categories (ints) to matrix of one hot columns
    
    categories : vector of length batch size of categpry labels (integers) 
    num_categories : number of different categories
    """
    encoded_categories = np.zeros((num_categories, len(categories)))
    encoded_categories[categories, np.arange(len(categories))] = 1
    return encoded_categories