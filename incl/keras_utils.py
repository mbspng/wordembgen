from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from multiprocessing import cpu_count

########## workaround for multi processing h5py bug ##########
# see https://github.com/keras-team/keras/issues/11101#issuecomment-473576166
import pickle
import numpy as np

MODEL_SAVE_NAME = 0
MODEL_SAVE_WEIGHTS = 1

def save_weight_light(model, filename):
    layers = model.layers
    pickle_list = []
    for layerId in range(len(layers)):
        weigths = layers[layerId].get_weights()
        pickle_list.append([layers[layerId].name, weigths])

    with open(filename, 'wb') as f:
        pickle.dump(pickle_list, f, -1)

def load_weight_light(model, filename):
    layers = model.layers
    with open(filename, 'rb') as f:
        pickle_list = pickle.load(f)

    for layerId in range(len(layers)):
        layers[layerId].set_weights(pickle_list[layerId][MODEL_SAVE_WEIGHTS])

class ModelCheckpointLight(Callback):
    def __init__(self, filepath, monitor='val_loss', verbose=0,
                 save_best_only=False,
                 mode='auto', period=1):
        super(ModelCheckpointLight, self).__init__()
        self.monitor = monitor
        self.verbose = verbose
        self.filepath = filepath
        self.save_best_only = save_best_only
        self.period = period
        self.epochs_since_last_save = 0

        if mode not in ['auto', 'min', 'max']:
            warnings.warn('ModelCheckpointLight mode %s is unknown, '
                          'fallback to auto mode.' % (mode),
                          RuntimeWarning)
            mode = 'auto'

        if mode == 'min':
            self.monitor_op = np.less
            self.best = np.Inf
        elif mode == 'max':
            self.monitor_op = np.greater
            self.best = -np.Inf
        else:
            if 'acc' in self.monitor or self.monitor.startswith('fmeasure'):
                self.monitor_op = np.greater
                self.best = -np.Inf
            else:
                self.monitor_op = np.less
                self.best = np.Inf

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            filepath = self.filepath.format(epoch=epoch + 1, **logs)
            if self.save_best_only:
                current = logs.get(self.monitor)
                if current is None:
                    warnings.warn('Can save best model only with %s available, '
                                  'skipping.' % (self.monitor), RuntimeWarning)
                else:
                    if self.monitor_op(current, self.best):
                        if self.verbose > 0:
                            print('\nEpoch %05d: %s improved from %0.5f to %0.5f,'
                                  ' saving model to %s'
                                  % (epoch + 1, self.monitor, self.best,
                                     current, filepath))
                        self.best = current
                        save_weight_light(self.model, filepath)
                    else:
                        if self.verbose > 0:
                            print('\nEpoch %05d: %s did not improve from %0.5f' %
                                  (epoch + 1, self.monitor, self.best))
            else:
                if self.verbose > 0:
                    print('\nEpoch %05d: saving model to %s' % (epoch + 1, filepath))
                save_weight_light(self.model, filepath)
########## /workaround for multi processing h5py bug ##########

def train_model(model, training_generator, checkpoint_path, history, epochs=1, validation_generator=None):
    """
    train `model` with samples from `training_generator`. `validation_generator` contains
    samples to guard against overfitting.
    """
    checkpoint = ModelCheckpoint(checkpoint_path,
                                 monitor='val_acc',
                                 verbose=1,
                                 save_best_only=False,
                                 mode='max')

    early_stopping = EarlyStopping(min_delta=.005, # stop training if improvement was less than half a percent
#                                    patience=0, # stop training immediatley
                                   patience=3, # stop training immediatley
                                   restore_best_weights=True
                                   )

    model.fit_generator(generator=training_generator,
                        validation_data=validation_generator,
                        use_multiprocessing=True,
                        workers=cpu_count(),
                        callbacks=[
                                   checkpoint,
                                   history,
#                                    early_stopping
                                  ],
                        max_queue_size=300,
                        epochs=epochs)

def train_model_cv(model, training_generator, checkpoint_path, history, epochs=1, validation_generator=None):
    """
    train `model` with samples from `training_generator`. `validation_generator` contains
    samples to guard against overfitting.
    """
    checkpoint = ModelCheckpoint(checkpoint_path,
                                 monitor='val_acc',
                                 verbose=1,
                                 save_best_only=True,
                                 mode='max')

    early_stopping = EarlyStopping(min_delta=.005, # stop training if improvement was less than half a percent
                                   patience=0, # stop training immediatley
                                   restore_best_weights=True
                                   )

    model.fit_generator(generator=training_generator,
                        validation_data=validation_generator,
#                         use_multiprocessing=True,
#                         workers=cpu_count(),
                        callbacks=[
                                   checkpoint,
                                   history
#                                    early_stopping
                                  ],
                        max_queue_size=300,
                        epochs=epochs)



from keras.models import Sequential
from keras.layers import Flatten, Dense, Conv1D, MaxPool1D, Dropout, Embedding
from keras.optimizers import SGD
from keras.initializers import glorot_normal
from keras.utils import multi_gpu_model


def build_model(embedding_matrix,
                text_length,
                trainable,
                output_dims,
                filter_cnt=256,
                dense_dims=1024,
                dropout_rate=.5):

    model = Sequential()

    vocab_size = embedding_matrix.shape[0]
    encoding_dims = embedding_matrix.shape[1]

    embedding_layer = Embedding(vocab_size, encoding_dims, input_length=text_length,
                                trainable=trainable, name='emb')
    embedding_layer.build((None,))
    embedding_layer.set_weights([embedding_matrix])
    model.add(embedding_layer)

    model.add(Conv1D(filters=filter_cnt, kernel_size=7, activation='relu', input_shape=(text_length, encoding_dims), bias_initializer=glorot_normal()))
    model.add(MaxPool1D(pool_size=3, strides=3))
    model.add(Conv1D(filters=filter_cnt, kernel_size=7, activation='relu', bias_initializer=glorot_normal()))
    model.add(MaxPool1D(pool_size=3, strides=3))
    model.add(Conv1D(filters=filter_cnt, kernel_size=3, activation='relu', bias_initializer=glorot_normal()))
    model.add(Conv1D(filters=filter_cnt, kernel_size=3, activation='relu', bias_initializer=glorot_normal()))
    model.add(Conv1D(filters=filter_cnt, kernel_size=3, activation='relu', bias_initializer=glorot_normal()))
    model.add(Conv1D(filters=filter_cnt, kernel_size=3, activation='relu', bias_initializer=glorot_normal()))
    model.add(MaxPool1D(pool_size=3, strides=3))
    model.add(Flatten())
    model.add(Dense(units=dense_dims, activation='relu', bias_initializer=glorot_normal()))
    model.add(Dropout(rate=dropout_rate))
    model.add(Dense(units=dense_dims, activation='relu', bias_initializer=glorot_normal()))
    model.add(Dropout(rate=dropout_rate))
    model.add(Dense(units=output_dims, activation='softmax', bias_initializer=glorot_normal()))

    try:
        model_mppu = multi_gpu_model(model, gpus=8)
        print('multi gpu model active')
    except Exception as e:
        model_mgpu = None
        print('single gpu model active')

    return model, model_mgpu
