import numpy as np
from sklearn.decomposition import PCA
import sklearn.preprocessing
import pickle


def embedding_txt2pickle(embedding_path, output_path, num_dims=300):
    
    embedding_matrix, word2index, index2word = load_embedding_from_txt(embedding_path, num_dims=300) 
    
    serialize_embedding(embedding_matrix, word2index, index2word, output_path)
        
        
def serialize_embedding(embedding_matrix, word2index, index2word, output_path):
    with open(output_path, 'wb') as f:
        pickle.dump((embedding_matrix, word2index, index2word), f)
        
        
def load_embedding_from_txt(embedding_path, num_dims=300):
    
    word2index = {}
    index2word = {}
    vectors = []

    with open(embedding_path, 'r') as f:
        i = 0
        for line in f:
            try:
                values = line.split()
                word = values[0]
                vec = np.asarray(values[1:], dtype='float32')
                assert(len(vec) == num_dims)
                vectors.append(vec)
                word2index[word] = i
                index2word[i] = word
                i+=1
            except Exception as e:
                print(e)

    embedding_matrix = np.vstack(vectors)
        
    return embedding_matrix, word2index, index2word
        
        
def load_embedding_from_pickle(serialized_embedding_path, num_vecs=-1):
    """
    num_vecs : if only a subset of the embedding is to be loaded, $num_vecs defines
               that subset as the number of vectors
    """
    with open(serialized_embedding_path, 'rb') as f:
        if num_vecs != -1:
            embedding_matrix, word2index_, index2word_ = pickle.load(f)
            embedding_matrix = embedding_matrix[:num_vecs]
            index2word = {}
            word2index = {}
            for i in range(num_vecs):
                index2word[i] = index2word_[i]
                word2index[index2word[i]] = i
            return embedding_matrix, word2index, index2word
        else:
            return pickle.load(f)
        
        
def indices_of_most_similar(idx, embeddings_matrix, n=20):
    """
    find the `n` words that are most similar to word_vector at index `i` in `embeddings`
    measured by their cosine similarity
    
    returns array of indices
    """ 
    v = embeddings_matrix[idx]
    m = embeddings_matrix
    cosines = (np.dot(v, m.T))/(np.linalg.norm(m.T, axis=0)*np.linalg.norm(v))
    ranked_by_similarity = np.argpartition(cosines, -n)[-n:]
    return reversed(ranked_by_similarity)


def most_similar_words(word, embeddings_matrix, word2index, index2word, n=20):
    
    most_similar = list(indices_of_most_similar(word2index[word], embeddings_matrix, n))
    return list(map(lambda i : index2word[i], most_similar))


def present_most_similar(word, most_similar):
    
    print(f'words most similar to "{word}":\n')
    print('rank \t | \t word\n')
    for rank, word in enumerate(most_similar):
        print(f'{rank} \t | \t {word}')