from keras.models import Sequential
from keras.utils import to_categorical, Sequence
from keras.preprocessing.sequence import skipgrams
from keras.preprocessing.sequence import make_sampling_table
import numpy as np
import random

from IPython.display import display, HTML


class DataGenerator(Sequence):
    def __init__(self, ngrams, batch_size=256, shuffle=True, negative_sample_rate=5):
        """
        ngrams  :  pd.Dataframe :  one  column  per  word  per ngram;  dtype  categorical;
        categories shared across columns; max(codes) = len(ngrams) codes are assumed to be
        continuous.
        """
        self.ngrams = ngrams
        self.batch_size = batch_size
        self.length = len(self.ngrams)//self.batch_size
        self.shuffle = shuffle

        # constants related to __getitem__
        self.vocab_size = len(ngrams.pos_1.cat.categories)
        self.context_size = len(ngrams.columns)//2
        self.center_index = self.context_size
        self.context_indices = list(range(
            self.context_size))+list(range(self.context_size+1, 2*self.context_size+1,))
        self.negative_sample_rate = negative_sample_rate
        self.pos_sample_count = self.batch_size*self.context_size*2
        self.neg_sample_count = self.batch_size*self.context_size*2*self.negative_sample_rate
        self.total_sample_count = self.pos_sample_count+self.neg_sample_count
        self.sampling_table = make_sampling_table(self.vocab_size)
        # display(self.ngrams.iloc[:10])

    def on_epoch_end(self):
        if self.shuffle:
            self.ngrams = self.ngrams.sample(frac=1).reset_index(drop=True)
            # display(self.ngrams.iloc[:10])

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        batch_ngrams = self.ngrams.iloc[idx*self.batch_size:idx *
                                        self.batch_size+self.batch_size]

        # keras word embedding layers expect input of the following shape
        # np.random.randint(vocab_size, size=(batch_size, ngram_size))

        batch_ngrams = np.array([col.cat.codes.values
                                 for _, col in batch_ngrams.iteritems()]).T

        # create positive and negative samples
        pairs_pos = []
        pairs_neg = []

        for ngram in batch_ngrams:
            center_word = ngram[self.center_index]
            context_words = ngram[self.context_indices]
            for context_word in context_words:
                pairs_pos.append([center_word, context_word])
                for _ in range(self.negative_sample_rate):
                    while True:
                        # definitly subject to improvement
                        non_context_word = random.randint(0, self.vocab_size-1)
                        if non_context_word not in context_words:
                            break
                    pairs_neg.append([center_word, non_context_word])

        samples = np.vstack([pairs_pos, pairs_neg])
        labels = np.hstack([np.ones(self.pos_sample_count, dtype=int), np.zeros(
            self.neg_sample_count, dtype=int)])

        batch = np.c_[samples.reshape(
            self.total_sample_count, -1), labels.reshape(self.total_sample_count, -1)]

        np.random.shuffle(batch)

        samples_view = batch[:, :samples.size //
                             len(samples)].reshape(samples.shape)
        labels_view = batch[:, samples.size //
                            len(samples):].reshape(labels.shape)

        return [samples_view[:,0], samples_view[:,1]], labels_view
