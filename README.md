# WordEmbGen

This repository contains code for training word embeddings by two methods: (1) Applying the SVD on a PPMI co-occurence matrix of skipgrams and (2) by training a simple word2vec model.

# Setup (Linux/macOS)
To obtain the source files and set up the environment navigate to any intended parent directory on your system and run:
```
git clone --depth=1 git@gitlab.com:mbspng/wordembgen.git
cd wordembgen
./setup.sh
```
This requires a working installation of [Anaconda](https://www.anaconda.com/distribution/).



# Usage

Activate the environment with `conda activate wordembgen`. For the Jupyter-notebooks run `jupyter-notebook` and run them in your browser.

First construct a training data set with *create_skipgrams.py* (I used the [*A Million Headlines* data set](https://www.kaggle.com/therohk/million-headlines)), then train word embedings with *embedding_with_svd.ipynb* and *embedding_with_word2vec.ipynb*. Then you can run evaluations with *evaluation.ipynb*.