#!/usr/bin/env python

from os import makedirs

embeddings_dir       = 'data/embeddings'
checkpoint_dir       = 'data/models/checkpoints'
completed_models_dir = 'data/models/completed'
training_history_dir = 'data/models/histories'
hdfstores_dir        = 'data/stores'
databases_dir        = 'data/models/databases'
testsets_dir         = 'data/testsets'

makedirs(embeddings_dir)
makedirs(checkpoint_dir)
makedirs(completed_models_dir)
makedirs(training_history_dir)
makedirs(hdfstores_dir)
makedirs(databases_dir)
makedirs(testsets_dir)